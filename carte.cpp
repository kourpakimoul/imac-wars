#include <iostream>
using namespace std;
#include "header.h"
#include <limits>

void pushUnit(Unit *unit) {
  tableau[(LARGEUR*unit->posY)+unit->posX] = unit->idUnit;  
}

void initTableau() {
  // (Ré)initialisation
  for (int i = 0; i < LONGUEUR*LARGEUR; i++) {
    tableau[i] = ".";
  };
}

void afficherCarte(){
  system("clear");
  initTableau();

  for (int i = 0; i < (int)joueurUn.unites.size(); i++) {
    if (joueurUn.unites[i].life > 0)
    {
      pushUnit(&(joueurUn.unites[i]));
    }
  }
  for (int i = 0; i < (int)joueurDeux.unites.size(); i++) {
    if (joueurDeux.unites[i].life > 0)
    {
      pushUnit(&(joueurDeux.unites[i]));
    }
  }

  cout << "\033[1;32m"; // color : bold blue
  cout << "|||||  0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  |" << endl;
  cout << "\033[0m"; // reset color

  for (int i = 0; i < LONGUEUR*LARGEUR; i++) {
    if (i%LARGEUR == 0) {
      if (i != 0){
        cout << endl;
      };
      cout << "\033[1;32m" ; // color : bold green
      cout << "-----------------------------------------------------------------" << endl;
      cout << " " << i/LARGEUR << " ||";
      cout << "\033[0m"; // reset color
    }
    if (tableau[i] == ".") {
      cout << "\033[1;32m" << "  " << tableau[i] << "  |" << "\033[0m";
    } else {
      if (tableau[i].at(0) == '1')
      {
        cout << "\033[1;31m" <<" " << tableau[i] << "\033[1;32m" << " |";
      }
      if (tableau[i].at(0) == '2')
      {
        cout << "\033[1;34m" <<" " << tableau[i] << "\033[1;32m" << " |";
      }
    }
  };
  cout<<endl<<endl;
}

// VÉRIFIER QUE LA CELLULE EST DISPONIBLE
bool checkCell(int cellx, int celly) {
  if (tableau[(celly*LONGUEUR)+cellx] == "." && cellx < 10 && celly < 10)
  {
    return true;
  } else {
    return false;
  }
  
}

void inputPos(int *posx, int *posy) {
  while(!(cin >> *posx >> *posy)){
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cout << "\033[1;31m" << "Entrée invalide ! Réessayez !" << "\033[0m" << endl;
      }
}
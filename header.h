#ifndef HEADER
#define HEADER

#include <vector>

const int LONGUEUR = 10;
const int LARGEUR = 10;
const int nbactions = 2; // Nombre d'actions que les joueurs peuvent exécuter par tour

struct Joueur;

// UNITÉ DE COMBAT
// pour l'instant 1 seul type d'unité
typedef struct Unit{ //structure de l'unité
  Joueur *joueur; //id du joueur
  int life; //points de vie
  float strength; //force de l'unité (coefficient)
  int posX; //cordonnée en x
  int posY; //coordonnée en y
  int dext; // dexterité --> distance parcourable en un tour
  string idUnit;
  char type;
  string typeName;
  int range; // Portée d'attaque
} Unit;

typedef struct Joueur{ //structure du joueur
  int id; //id du joueur
  string name; //nom du joueur
  vector<Unit> unites; // tableau des unites du joueur
  Joueur *adversaire;
  int money;
} Joueur ;

// carte.ccp
void afficherCarte();
void pushUnit(Unit *unit);
bool checkCell(int cellx, int celly);
void initTableau();
void inputPos(int *posx, int *posy);

// menu.cpp
void entreeJeu();
void displayInfos(Joueur *player);
void endGame(Joueur *looser);
void displayMoney(Joueur *joueur);
//void partie();

// joueurs.cpp
void entrerJoueur(Joueur *joueur);
void afficherJoueur(Joueur joueur);
void entrerUnite(Joueur *joueur);
void initUnites(Joueur *joueur);
bool joueurAttaque(Joueur *joueurAtt, Joueur *joueurCible);
void playerTurn(Joueur *player);
bool playerIsDead(Joueur *player);
//bool playerCanAttack(Joueur *player);

// unit.cpp
void deplacement(Joueur *joueur, string tab[]);
Unit *selectUnit(Joueur *player);
void unitAttackUnit(Unit *attaquant,Unit *cible);
void moveUnit(Unit *unit);
int nombreDeCases(int xInit, int yInit, int xFin, int yFin);
bool unitIsDead(Unit *unit);
void chooseUnit(Unit *unit);

// TABLEAU POUR GÉRER LA CARTE
extern string tableau[LONGUEUR*LARGEUR];
extern Joueur joueurUn;
extern Joueur joueurDeux;

#endif

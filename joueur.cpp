#include <iostream>
using namespace std;
#include "header.h"

void entrerJoueur(Joueur *joueur){
    cout << "\033[0;36m" <<"Entrez un pseudo pour le joueur :" << "\033[0m" <<endl;
    cin >> joueur->name;
}

void afficherJoueur(Joueur joueur){
  cout << joueur.id <<" "<<joueur.name<<endl<<endl;
}

// Initialiser les unités du joueur
void initUnites(Joueur *joueur){
  do {
    system("clear");
    afficherCarte();

    displayMoney(joueur);

    cout << "\033[1;36m" << joueur->name << " voulez vous acheter une nouvelle unité ? (O/N)\n(vous devez acheter au moins 1 unité)" << "\033[0m" <<endl;
    char choice;
    cin >> choice;
    if (choice == 'O' || choice == 'o')
    {
      Unit tampon;
      tampon.joueur = joueur;
      chooseUnit(&tampon);
      tampon.idUnit = to_string(joueur->id)+tampon.type+to_string(joueur->unites.size());
      joueur->unites.push_back(tampon);
    }
    else if(choice=='N' || choice=='n')
    {
      if (joueur->unites.size()>0)
      {
        break; 
      }
    } else {
      cout << "\033[1;31m" << "Choix invalide !" << endl;
    }
  }while (joueur->money > 0);
}

// Le joueur attaque
bool joueurAttaque(Joueur *joueur) {
  Unit *unitAtt;
  Unit *unitCible;
  bool ok = false;
  
  while (ok == false)
  {
    cout << joueur->name << " vous attaquez." << "\n" << "UNITE ATTAQUANTE : ";
    unitAtt = selectUnit(joueur);

    // Annulation
    if (unitAtt == NULL)
    {
      break;
    }

    if (unitAtt->life > 0)
    {
      while (unitCible->life <= 0)
      {
        cout << "UNITE CIBLE : ";
        unitCible = selectUnit(joueur->adversaire);
        // Annulation
        if (unitCible == NULL)
        {
          ok = true;
          break;
        }
      }

      if (unitCible != NULL)
      {
        while (nombreDeCases(unitAtt->posX,unitAtt->posY,unitCible->posX,unitCible->posY)>unitAtt->range)
        {
          if (nombreDeCases(unitAtt->posX,unitAtt->posY,unitCible->posX,unitCible->posY) > unitAtt->range){
            cout<<"Vous n'êtes pas assez proche de la cible, recommencez"<<endl;
            joueurAttaque(joueur);
          }
        } 

        unitAttackUnit(unitAtt, unitCible);

        ok = true;
        return true;
      } else {
        ok = true;
        break;
      }
    }
    else {
      cout << "\033[1;31m" << "Cette unité n'est plus en vie !" << "\033[0m" << endl;
    }
  }
}

// TOUR DE JEU D'UN JOUEUR
void playerTurn(Joueur *player){
  afficherCarte();
  cout << "\033[1;35m" << " ----- TOUR DE " << player->name << "-----" <<  "\033[0m" << endl;
  displayInfos(player);

  int step = 0;
  char choice;

  while (step < nbactions)
  {
    afficherCarte();
    cout << "\033[1;35m" << " ----- TOUR DE " << player->name << "-----" <<  "\033[0m" << endl;
    displayInfos(player);

    cout << "\033[0;36m";
    cout << "Action ? \n -> Attaquer (A) || Deplacer une unite (D) || Passer le tour (P)" << endl;  
    cout << "\033[0m";

    cin >> choice;

    if (choice == 'A' || choice == 'a')
    {
      if (joueurAttaque(player))
      {
        step++;
      }
    }
    else if (choice == 'D' || choice == 'd')
    {
      bool ok = false;
      while (ok == false)
      {
        Unit *unit = selectUnit(player);
        if (unit != NULL)
        {
          if (unit->life > 0)
          {
            moveUnit(unit);
            step++;
            ok = true;
          } 
          else {
            cout << "\033[1;31m" << "Cette unité n'est plus en vie !" << "\033[0m" << endl;
          }
        } 
        else {
          ok = true;
        }
      }
    }
    else if (choice == 'P' || choice == 'p')
    {
      cout << "\n Tour passé \n";
      system("clear");
      step = nbactions;
    } 
    else {
      cout << "\033[1;31m" << "Cela ne correspond à aucune action ! \n" << "\033[0m";
      cout<<"Veuillez choisir une des propositions"<<endl;
    }
  }
  // Lancer le tour de l'adversaire
  // Si l'adversaire n'est pas mort
  if (!playerIsDead(player->adversaire))
  {
    playerTurn(player->adversaire);
  }
}

bool playerIsDead(Joueur *player) {
  for (int i = 0; i < (int)player->unites.size(); i++)
  {
    if (player->unites[i].life > 0)
    {
      return false;
    } 
  }
  // cout << "playerIsDead() returned true \n";
  cout << "\033[1;31m" << player->name << " n'a plus d'unité en vie !\n" << player->adversaire->name << " a gagné !\nBRAVO" << endl;
  endGame(player);
  return true;
}
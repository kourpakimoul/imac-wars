all: imacwars

imacwars : main.o carte.o joueur.o menu.o unit.o
	gcc main.o carte.o joueur.o menu.o unit.o -o imacwars -lstdc++
	@echo Compilation terminee

main.o : main.cpp
	gcc -c -Wall main.cpp

carte.o : carte.cpp
	gcc -c -Wall carte.cpp
joueur.o : joueur.cpp
	gcc -c -Wall joueur.cpp
menu.o : menu.cpp
	gcc -c -Wall menu.cpp
unit.o : unit.cpp
	gcc -c -Wall unit.cpp

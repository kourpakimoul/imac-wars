#include <iostream>
using namespace std;
#include "header.h"

void entreeJeu(){
system("clear");
cout << "\033[1;36m" <<"*******************************************************************************************\n"<<endl;
cout << " /$$$$$$ /$$      /$$  /$$$$$$   /$$$$$$        /$$      /$$  /$$$$$$  /$$$$$$$   /$$$$$$ "<<endl;
cout << "|_  $$_/| $$$    /$$$ /$$__  $$ /$$__  $$      | $$  /$ | $$ /$$__  $$| $$__  $$ /$$__  $$"<<endl;
cout << "  | $$  | $$$$  /$$$$| $$  \\ $$| $$  \\__/      | $$ /$$$| $$| $$  \\ $$| $$  \\ $$| $$  \\__/"<<endl;
cout << "  | $$  | $$ $$/$$ $$| $$$$$$$$| $$            | $$/$$ $$ $$| $$$$$$$$| $$$$$$$/|  $$$$$$ "<<endl;
cout << "  | $$  | $$  $$$| $$| $$__  $$| $$            | $$$$_  $$$$| $$__  $$| $$__  $$ \\____  $$"<<endl;
cout << "  | $$  | $$\\  $ | $$| $$  | $$| $$    $$      | $$$/ \\  $$$| $$  | $$| $$  \\ $$ /$$  \\ $$"<<endl;
cout << " /$$$$$$| $$ \\/  | $$| $$  | $$|  $$$$$$/      | $$/   \\  $$| $$  | $$| $$  | $$|  $$$$$$/"<<endl;
cout << "|______/|__/     |__/|__/  |__/ \\______/       |__/     \\__/|__/  |__/|__/  |__/ \\______/ "<<endl<<endl;
cout << "                created by Mattéo Popoff & Alaric Rougnon-Glasson ®2020\n" << endl;
cout << "*******************************************************************************************\n\nToute ressemblance avec des personnes existantes ou ayant existé ne serait que purement fortuite.\n\n"<< "\033[0m" <<endl;


    char menu;

    cout<<"Voulez-vous lire les règles du jeu ? (o/n) ";
    cin>>menu;
    if(menu=='o' || menu=='O')
        {
            cout<<endl<<"\033[1;36m"<<"Les règles sont simples : "<<endl<<endl<<"Chaque joueur achète des unités militaires."<<endl<<"Chacun leur tour, ils selectionnent une de leurs unités sur la grille, puis la déplacent, la font attaquer ou passent leur tour."<<endl<<"Lorsqu'un des joueurs a détruit toutes les unités adverses, il est proclamé gagnant.\n\nIl existe 3 types de parties : \n-Ultra : chaque joueur dispose de 4000$ pour acheter des unités\n-Normal : chaque joueur dispose de 2500$ pour acheter des unités\n-Simple : chaque joueur dispose de 1500$ pour acheter des unités"<<"\033[0m"<<endl<<endl;
        }
    else if(menu=='n'|| menu=='N'){
        cout<<endl<<"Bonne partie !"<<endl;
    }
    else{
        cout<<menu<<" n'était pas une réponse possible, tant pis pour les règles"<<endl;
    }

    cout<<endl<<"\033[0;36m"<<"Veuillez choisir les noms des participants"<< "\033[0m" <<endl;
    cout<< "\033[1;31m" << "Joueur 1"<<endl;
    entrerJoueur(&joueurUn);
    cout<<endl;
    cout<< "\033[1;34m"<<"Joueur 2"<<endl;
    entrerJoueur(&joueurDeux);

    system("clear");

    cout<<"Liste des joueurs"<<endl<<endl;
    afficherJoueur(joueurUn);
    afficherJoueur(joueurDeux);

    // On initialise l'adversaire des joueurs
    joueurUn.adversaire = &joueurDeux;
    joueurDeux.adversaire = &joueurUn;
    
    bool done = false;
    while (done == false)
    {
        char diff;
        cout<< "\033[1;34m"<<"Choisir type de partie :\n Ultra (U) | Normal (N) | Simple (S)"<<endl;
        cin >> diff;
        if (diff=='U' || diff == 'u')
        {
            joueurUn.money = 4000;
            joueurDeux.money = 4000;
            done = true;
        }
        else if (diff=='N' || diff == 'n')
        {
            joueurUn.money = 2500;
            joueurDeux.money = 2500;
            done = true;
        }
        else if (diff == 'S' || diff == 's')
        {
            joueurUn.money = 1500;
            joueurDeux.money = 1500;
            done = true;
        }
        else {
            cout << "\033[1;31m" << "Choix invalide ! Réessayez !" << "\033[0m" << endl;
        }
    }    

    // On initialise le tableau et les unités des joueurs
    initTableau();
    initUnites(&joueurUn);
    initUnites(&joueurDeux);
    
    // Lancement de la partie
    afficherCarte();
    playerTurn(&joueurUn);
}

// AFFICHER LES INFOS SUR LA PARTIE EN COURS
void displayInfos(Joueur *player) {
    if (player->id == 1)
    {
        cout << "\033[0;31m";
    } else {
        cout << "\033[0;34m";
    }
    cout << "UNITES DE "<< player->name << endl;
    for (int i = 0; i < (int)player->unites.size(); i++)
    {
        if (player->unites[i].life > 0)
        {
            cout << " » " << player->unites[i].idUnit << " » ♥ : " << player->unites[i].life << " | DEXTÉRITÉ : " << player->unites[i].dext << " | PORTÉE : " << player->unites[i].range << " | FORCE : " << player->unites[i].strength << endl;
        } else {
            cout << " » " << player->unites[i].idUnit << " » MORTE" << endl;
        }
    }
    if (player->id == 1)
    {
        cout << "\033[0;34m";
    } else {
        cout << "\033[0;31m";
    }
    cout << "UNITES DE "<< player->adversaire->name << endl;
    for (int i = 0; i < (int)player->adversaire->unites.size(); i++)
    {
        if (player->adversaire->unites[i].life > 0)
        {
            cout << " » " << player->adversaire->unites[i].idUnit << " » ♥ : " << player->adversaire->unites[i].life << " | DEXTÉRITÉ : " << player->adversaire->unites[i].dext << " | PORTÉE : " << player->adversaire->unites[i].range << " | FORCE : " << player->adversaire->unites[i].strength << endl;
        } else {
            cout << " » " << player->adversaire->unites[i].idUnit << " » MORTE" << endl;
        }
    }
    cout << "\033[0m";
}

// FIN DU JEU
void endGame(Joueur *looser){
    char reponse;
    cout << "\n\n" << "\033[1;35m" << looser->adversaire->name << " a gagne ! \n" << endl;
    cout << "\033[0m" << "Jeu terminé." << endl <<endl;
    cout << "Voulez vous rejouer ? oui (o) ou non (n)"<<endl;
    cin >> reponse;
    if (reponse=='o'){
        vector<Unit> swap;
        joueurUn.unites.clear();
        joueurUn.unites.swap(swap);
        joueurDeux.unites.clear();
        joueurDeux.unites.swap(swap);
        entreeJeu();
    }
    else 
        exit(1);
}

void displayMoney(Joueur *joueur) {
    cout << "\033[0;33m" << joueur->name << " : " << joueur->money << "$ restants" << "\033[0m" << endl;
}
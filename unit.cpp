#include <iostream>
using namespace std;
#include "header.h"

Unit *selectUnit(Joueur *player){
  string id;
  bool found = false;
  while (found == false)
  {
    cout << "\033[0;36m" << "Sélectionnez une unité en entrant son ID (C pour annuler): "<< "\033[0m" << endl;
    cin >> id;
    // Annulation de la selection (à gérer dans les fonctions appelantes)
    if (id == "C" || id == "c")
    {
        cout << "Annulation" << endl;
        return NULL;
    }

    id[1] = toupper(id[1]);

    for (int i = 0; i < (int)player->unites.size(); i++) {
        if (player -> unites[i].idUnit == id)
        {
            return &(player->unites[i]);
            found = true;
        }
    }
    if (found == false)
    {
        cout << "\033[1;31m" << "Impossible de trouver une unité correspondant à cet ID !" << "\033[0m" << endl;
    } 
  }
}

void unitAttackUnit(Unit *attaquant, Unit *cible) {
    cible->life -= (attaquant->strength)*(attaquant->life); // La cible prend des dégats
    attaquant->life -= (cible->strength)*(abs(cible->life)); // L'attaquant prend des dégats. On utilise un valeur absolue pour éviter que l'attaquant gagne des la vie quand il tue une unité
    if (unitIsDead(cible))
    {
        tableau[LARGEUR*(cible->posY)+cible->posX] = "."; // Supprimer l'unité du tableau si elle est morte
        afficherCarte();
        playerIsDead(cible->joueur);        
    }
    else if (unitIsDead(attaquant))
    {
        tableau[LARGEUR*(attaquant->posY)+cible->posX] = "."; // Supprimer l'unité du tableau si elle est morte
        afficherCarte();
        playerIsDead(attaquant->joueur);
    }
}

void moveUnit(Unit *unit){
    int posx, posy;
    bool check = false;
    while (check != true)
    {
    cout << "\033[0;36m" << "Entrez la nouvelle position :" << "\033[0m" << endl;
    inputPos(&posx, &posy);
    if (checkCell(posx, posy) && nombreDeCases(unit->posX,unit->posY,posx,posy) <= unit->dext){
        check = true;
        unit->posX = posx;
        unit->posY = posy;
        afficherCarte(); 
    } else if (nombreDeCases(unit->posX,unit->posY,posx,posy) > unit->dext) {
        cout << "\033[1;31m" << "Cette unité ne peut pas se déplacer de plus de "<< unit->dext << " cases ! Réessayez !" << "\033[0m" << endl;
    }
    else {
        cout << "\033[1;31m" << "Impossible de placer l'unité sur cette cellule !"<< "\033[0m" << endl;
    }        
    }  
}

bool unitIsDead(Unit *unit) {
    if (unit->life > 0)
    {
        return false;
    } else {
        cout << "Unit " << unit->idUnit << " is dead." << endl;
        return true;
    }
}

int nombreDeCases(int xInit, int yInit, int xFin, int yFin){
    int nombre = abs((xInit+yInit)-(xFin+yFin));
    return nombre;
}

void chooseUnit(Unit *unit) {
    bool type = false;
    while (type == false)
    {
        cout << "\033[0;36m" << "Choisissez le type de l'unité (entrez un caractère): \nI - Infanterie (100$) | T - Tank (700$) | S - Sniper (500$) | B - Bombardier (400$) | C - CHERRIER Sylvain (2000$) | V - VINCENT Steeve (2000$)"<< "\033[0m" << endl;
        cin >> unit->type;
        unit->type = toupper(unit->type);
        if ((unit->type == 'I' ||  unit->type == 'i') && unit->joueur->money >= 100) // INFANTERIE
        {
            unit->life = 70;
            unit->strength = 0.2;
            unit->range = 2;
            unit->dext = 3;
            unit->joueur->money -= 100;
            unit->typeName = "Infanterie";
            type = true;
        }
        else if ((unit->type == 'T' || unit->type == 't') && unit->joueur->money >= 700) { // TANK
            unit->life = 200;
            unit->strength = 0.4;
            unit->range = 3;
            unit->dext = 1;
            unit->joueur->money -= 700;
            unit->typeName = "Tank";
            type = true;
        } 
        else if ((unit->type == 'S' || unit->type == 's') && unit->joueur->money >= 500) { // SNIPER
            unit->life = 60;
            unit->strength = 0.6;
            unit->range = 6;
            unit->dext = 1;
            unit->joueur->money -= 500;
            unit->typeName = "Sniper";
            type = true;
        }
        else if ((unit->type == 'B' || unit->type == 'b') && unit->joueur->money >= 400) { // BOMBARDIER
            unit->life = 100;
            unit->strength = 0.3;
            unit->range = 4;
            unit->dext = 2;
            unit->joueur->money -= 400;
            unit->typeName = "Bombardier";
            type = true;
        }
        else if ((unit->type == 'C' || unit->type == 'c') && unit->joueur->money >= 2000) { // SYLVAIN CHERRIER - Injouable en en mode simple
            unit->life = 300;
            unit->strength = 0.8;
            unit->range = 5;
            unit->dext = 5;
            unit->joueur->money -= 2000;
            unit->typeName = "Sylvain Cherrier";
            type = true;
        }
        else if ((unit->type == 'V' || unit->type == 'v') && unit->joueur->money >= 2000) { // STEEVE VINCENT - Injouable en en mode simple aussi
            unit->life = 300;
            unit->strength = 0.8;
            unit->range = 5;
            unit->dext = 5;
            unit->joueur->money -= 2000;
            unit->typeName = "Steeve Vincent";
            type = true;
        } 
        else {
            cout << "\033[1;31m" << "Impossible d'acheter l'unité ! Vérifier que vous avez fait un choix valide et que vous avez assez d'argent.\n" << "\033[0m" << endl;
        }
    }

    bool check = false;
    int posx, posy;

    while (check == false)
    {
      cout << "\033[0;36m" << "Entrez les coordonnees de l' unite (X puis Y)" << "\033[0m" << endl;

      inputPos(&posx, &posy);
      
      if (checkCell(posx, posy) == true)
      {
        unit->posX = posx;
        unit->posY = posy;
        check = true;
        pushUnit(unit);
      } else {
        cout << "\033[1;31m" << "Impossible de placer l'unité sur cette case ! \n" << "\033[0m";
      }
    } 
}